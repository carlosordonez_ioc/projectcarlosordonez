package ioc.xtec.cat.calculadora;

import java.util.Scanner;

// Carlos Ordonez Ortiz He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!i moooolt pessat!!!

/**
 * Classe que implementa una calculadora bàsica per realitzar operacions matemàtiques.
 * Permet a l'usuari introduir dos nombres i realitzar operacions de suma, resta,
 * multiplicació i divisió.
 * 
 * @author Carlos Ordonez Ortiz EAC4 M08
 * @version 1.0
 */

public class Calculadora {
    
    /**
     * El punt d'entrada principal del programa.
     * 
     * @param args arguments de la línia de comandes (no utilitzats)
     */

    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + Operacions.sumar(num1, num2));
        System.out.println("Resta: " + Operacions.restar(num1, num2));
        System.out.println("Multiplicació: " + Operacions.multiplicar(num1, num2));
/**
     * El punt d'entrada principal del programa.
     * 
     * @param args arguments de la línia de comandes (no utilitzats)
     */
        try {
            System.out.println("Divisió: " + Operacions.dividir(num1, num2));
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            scanner.close(); 
        }
    }
    
}
