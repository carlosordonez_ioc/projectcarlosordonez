package ioc.xtec.cat.calculadora;

/**
 * Classe que proporciona mètodes estàtics per realitzar operacions aritmètiques bàsiques,
 * com suma, resta, multiplicació i divisió.
 * Aquesta classe és utilitzada per la classe {@link Calculadora} per realitzar càlculs.
 * 
 * @author Carlos Ordonez Ortiz EAC4 M08
 * @version 1.0
 */
public class Operacions {

    /**
     * Realitza la suma de dos nombres.
     * 
     * @param a El primer operand.
     * @param b El segon operand.
     * @return La suma de a i b.
     */
    public static double sumar(double a, double b) {
        return a + b;
    }

    /**
     * Realitza la resta entre dos nombres.
     * 
     * @param a El primer operand.
     * @param b El segon operand.
     * @return La diferència entre a i b.
     */
    public static double restar(double a, double b) {
        return a - b;
    }

    /**
     * Realitza la multiplicació de dos nombres.
     * 
     * @param a El primer operand.
     * @param b El segon operand.
     * @return El producte de a i b.
     */
    public static double multiplicar(double a, double b) {
        return a * b;
    }

    /**
     * Realitza la divisió entre dos nombres.
     * 
     * @param a El primer operand.
     * @param b El segon operand.
     * @throws ArithmeticException Si el segon operand (divisor) és zero.
     * @return El quocient de a dividit per b.
     */
    public static double dividir(double a, double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        }
        return a / b;
    }
}

